typedef enum {RESET, MULT_STAGE, CHECK, DONE} STATE;

module ISR(
	input                reset,
	input                clock,
	input         [63:0] value,
	output logic  [31:0] result,
	output logic	     done
  
  );

	STATE		state, n_state;
	logic 	[63:0]	in_value, guess_sq;
	logic	[31:0]	guess, prev_guess, current_value, shift_value, prev_shift,result_out;
	logic		start_mult, done_mult;
	assign done = (state ==DONE) ? 1'b1: 1'b0;
	assign result = (state ==DONE) ? current_value : 32'h0 ;
	mult m0(	.clock(clock),
			.reset(reset),
			.mcand({32'b0,guess}),
			.mplier({32'b0,guess}),
			.start(start_mult),
			.product(guess_sq),
			.done(done_mult));
	  // Next-state logic
	always_comb begin 
		case(state)
			RESET: 
				begin
				in_value = value;
				guess = 32'h80000000;
				shift_value = 32'h80000000;
				current_value = 32'h0;
				n_state = MULT_STAGE;
				start_mult = 1'b1;
				end
			MULT_STAGE:
				begin
				start_mult = 1'b0;
				if (done_mult) begin
					n_state = CHECK;
					prev_guess = guess;
					prev_shift = shift_value;
				end else begin
					n_state = MULT_STAGE;
				end
				end
			CHECK:
				begin
				start_mult = 1'b0;
				if (shift_value == 32'h0) begin
					n_state = DONE;
				end else begin
					if ( guess_sq <= in_value ) begin
						current_value = prev_guess;
					end 
					shift_value = prev_shift >> 1;
					guess = current_value ^ shift_value;
					n_state = MULT_STAGE;
					start_mult = 1'b1;
				end
				end
			DONE:
				begin
				start_mult = 1'b0;
				n_state = RESET;
				end
			default: n_state = RESET;
		endcase
	end
	
	always_ff @(posedge clock) begin
		if (reset) begin
			state <= #1 RESET;
			result_out <= #1 32'h0;
		end else begin
			state <= #1 n_state;
		end
	end

endmodule
