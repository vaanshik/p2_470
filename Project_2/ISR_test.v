///////////////////////////////////////////////////////////////////////
//
// Uniquename 	: vaanshik
// File		: ISR Testbench
// Date		: September 19th, 2020
//
// ////////////////////////////////////////////////////////////////////

module isr_testbench();
	
	logic [63:0] a;
	logic [31:0] out;
	logic reset, clock, done, check;

	logic [31:0] tb_sqrt;
	logic correct = 1'b0;

        task calc_sqrt;
		input [63:0] a;
		begin	
			tb_sqrt = 32'h0000_0000;
	        	for (int i = 31; i >= 0; i = i - 1) 
	        	begin
	        		tb_sqrt[i] = 1'b1;
	        		if (tb_sqrt*tb_sqrt > a)
	        		begin
	        			tb_sqrt[i] = 1'b0;
	        		end	
	        	end
		end
	endtask

	ISR I0(
		.clock(clock),
		.reset(reset),
		.value(a),
		.result(out),
		.done(done)
		);

	always @ (posedge clock)
	begin
		if (check) 
		begin
			correct = (tb_sqrt == out)? 1'b1: 1'b0;
		   if (!correct)
		   begin
		   	$display("@@@ FAILED");
		   	$display("Incorrect Value at Time = %4.0f ", $time);
		   	$display("Correct =%h, A = %h, Output = %h, Expected Output = %h", correct, a, out, tb_sqrt);
		   	$finish;
		   end
		   check = 0;
		end
	end

	always 
	begin
		#5;
		clock =~clock;
	end

        task wait_until_done;
                forever 
		begin : wait_loop
                        @(posedge done);
                        @(negedge clock);
                        if(done) 
				disable wait_until_done;
                end
        endtask

	initial
	begin
		$monitor("TIME: %4.0f   || RST: %b || DONE: %b || VALUE: 64'h%h (%-3d) || RESULT: 32'h%h (%-3d)", $time, reset, done,a,a,out,out);
		// [a] : Simple Testing
		clock = 0;
		@(negedge clock);
		reset = 1;
		@(negedge clock);
		a = 101;
		reset = 0;
		wait_until_done();
		$display("Correct =%h, A = %h, Output = %h, Expected Output = %h", correct, a, out, tb_sqrt);
		calc_sqrt(a);
		$display("Correct =%h, A = %h, Output = %h, Expected Output = %h", correct, a, out, tb_sqrt);
		check = 1;
		$display("Correct =%h, A = %h, Output = %h, Expected Output = %h", correct, a, out, tb_sqrt);
	/*	@(negedge clock);
		a = 40;
		wait_until_done();
		calc_sqrt(a);
		check = 1;
		@(negedge clock);
		a = 49;
		wait_until_done();
		calc_sqrt(a);
		check = 1;
		@(negedge clock);
		a = 100;
		wait_until_done();
		calc_sqrt(a);
		check = 1;
	
		// [b] : Short Loops
		for (int i = 1000; i < 10000; i = i + 100)
		begin
		       @(negedge clock);
			a = i;
		        wait_until_done();
		        calc_sqrt(a);
		        check = 1;
		end

		// [c] : Short Loops : Perfect Squares
		for (int i = 20000; i < 1000000; i = i + 100) begin
		       @(negedge clock);
			a = i*i;
		        wait_until_done();
		        calc_sqrt(a);
		        check = 1;
		end
		
		// [d] : Random Testing
		for (int i = 0; i < 99; i = i + 1)
		begin
		       @(negedge clock);
			a = {$random, $random};
		        wait_until_done();
		        calc_sqrt(a);
		        check = 1;
		end
		
		// [e] : Corner cases
		@(negedge clock);
		a = 0;
		wait_until_done();
		calc_sqrt(a);
		check = 1;
		@(negedge clock);
		a = 1;
		wait_until_done();
		calc_sqrt(a);
		check = 1;
		
		// [f] : Reset testing
		@(negedge clock);
		a = 3600;
		for (int i = 0; i < 200; ++i)
			@(negedge clock);
		reset = 1;
		for (int i = 0; i < 10; ++i)
			@(negedge clock);
		reset = 0;
		wait_until_done();
		calc_sqrt(a);
		check = 1;
		@(negedge clock);
		a = 64;
		for (int i = 0; i < 50; ++i)
			@(negedge clock);
		reset = 1;
		for (int i = 0; i < 10; ++i)
			@(negedge clock);
		a = 100;
		reset = 0;
		for (int i = 0; i < 50; ++i)
			@(negedge clock);
		reset = 1;
		for (int i = 0; i < 10; ++i)
			@(negedge clock);
		reset = 0;
	wait_until_done();
		calc_sqrt(a);
		check = 1;
	*/	@(negedge clock);
		$display("TESTING FINISHED!!!");
		$display("@@@ PASSED");
		$finish;
	end
endmodule
