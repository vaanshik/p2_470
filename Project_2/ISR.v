typedef enum {RESET, MULT_STAGE, CHECK, DONE} STATE;

module ISR(
	input                reset,
	input                clock,
	input         [63:0] value,
	output logic  [31:0] result,
	output logic	     done
  
  );

	STATE		state, n_state;
	logic 	[63:0]	in_value, guess_sq;
	logic	[31:0]	guess, guess_next, shift_value, shift_value_next, result_out;
	logic		start_mult, done_mult;

// DONE signal logic
	assign done = (state ==DONE) ? 1'b1: 1'b0;
	assign result = (state ==DONE) ? result_out : 32'h0 ;

// multiplier instantiated
	mult m0(	.clock(clock),
			.reset(reset),
			.mcand({32'b0,guess}),
			.mplier({32'b0,guess}),
			.start(start_mult),
			.product(guess_sq),
			.done(done_mult));

// Next-state logic
	always_comb begin
		shift_value = shift_value_next;
		guess = guess_next;
		case(state)
			RESET: 
				begin
				in_value = value;
				guess = 32'h80000000;
				shift_value = 32'h80000000;
				n_state = MULT_STAGE;
				start_mult = 1'b1;
				end
			MULT_STAGE:
				begin
				start_mult = 1'b0;
				if (done_mult) begin
					n_state = CHECK;
				end else begin
					n_state = MULT_STAGE;
				end
				end
			CHECK:
				begin
				start_mult = 1'b0;
				if (shift_value == 32'h0) begin
					n_state = DONE;
				end else begin
					if ( guess_sq > in_value ) begin
						guess = guess ^ shift_value;	// masking or discarding current bit	
					end 
					shift_value = shift_value >> 1;		// shifting current bit to right by 1 position
					guess = guess | shift_value;
					n_state = MULT_STAGE;
					start_mult = 1'b1;
				end
				end
			DONE:
				begin
				start_mult = 1'b0;
				n_state = RESET;
				end
			default: n_state = RESET;
		endcase
	end
	
	always_ff @(posedge clock) begin
		if (reset) begin			// reset signal conditions
			state <= #1 RESET;
			result_out <= #1 32'h0;
			shift_value_next <= #1 32'h80000000;
		end else begin
			if ((state == RESET))
				begin
				shift_value_next <= #1 32'h80000000;
				guess_next <= #1 32'h80000000;
				end
			if ((state == CHECK))
				begin
				shift_value_next <= #1 shift_value;
				guess_next <= #1 guess;
				result_out <= #1 guess_next;
				end
			state <= #1 n_state;
		end
	end
endmodule
