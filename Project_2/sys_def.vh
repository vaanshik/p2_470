/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  sys_defs.vh                                         //
//                                                                     //
//  Description :  This file has the parameter used for pipeline       //
//		   multiplication in project 2			       //
//                 		                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

parameter DATA_WIDTH = 64;
parameter PIPE_DEPTH = 2;
parameter PIPE_WIDTH = (DATA_WIDTH/PIPE_DEPTH);
